---
title: Compose Layouts
---

## Introduction

Arranging your Composables can be tricky to get right. Jetpack Compose's included layouts give you the basic support you need for most layouts, but you can gain more control and simpler specification with a custom layout. We'll also see how a "slotted" layout allows you to define a common overall structure where you can plug in each section of the layout.

In this module, we'll explore different ways to create a simple "Forms" interface, starting with basic Rows and Columns, trying out ConstraintLayout, then creating our own layout manager that will adapt to show a form differently based on available space.


## Videos

Total video time for this module: 2:17:34

            
#### Example: Basic and Custom Layouts (Fall 2021) (1:11:20)

<iframe width="800" height="450" src="https://www.youtube.com/embed/V-rrPANU264" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>               

#### Lecture: Slot APIs (Fall 2021) (16:47)

<iframe width="800" height="450" src="https://www.youtube.com/embed/3XsCVqcdzVA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Example: Slot APIs (Fall 2021) (10:27)

<iframe width="800" height="450" src="https://www.youtube.com/embed/uQIUpHP50Tc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### (Bonus, Not Required) More detail on Template Method, Strategy and Null Object Patterns (Fall 2021) (39:00)

<iframe width="800" height="450" src="https://www.youtube.com/embed/EWBCy0jWFvc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
## Example Source

Sample Code: [https://gitlab.com/605-686/fall-2023-refresh/modules/compose-layouts](https://gitlab.com/605-686/fall-2023-refresh/modules/compose-layouts)
